-- "features" chatcommand extracted from tunneler's abyss misc mod.
-- Written by Hume2, MIT licensed. I DID NOT WRITE THIS CODE.

local function mylength(table)
	local foo = 0
	for _ in pairs(table) do foo = foo + 1 end
	return foo
end

minetest.register_chatcommand("features", {
   description = "Display counts of registered features",
   privs = {},
   params = "",
   func = function(name, param)
      minetest.chat_send_player(name, string.format("Registered nodes: %d", mylength(minetest.registered_nodes)))
      minetest.chat_send_player(name, string.format("Registered craftitems: %d", mylength(minetest.registered_craftitems)))
      minetest.chat_send_player(name, string.format("Registered tools: %d", mylength(minetest.registered_tools)))
      minetest.chat_send_player(name, string.format("Registered entities: %d", mylength(minetest.registered_entities)))
      minetest.chat_send_player(name, string.format("Registered ABMs: %d", #(minetest.registered_abms)))
      minetest.chat_send_player(name, string.format("Registered LBMs: %d", #(minetest.registered_lbms)))
   end,
})

